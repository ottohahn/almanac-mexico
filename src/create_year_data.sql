/* PROGRAM create_year_data.sql
   AUTHOR Otto Hahn Herrera
   DATE 2023-06-21
   PURPOSE To create a procedure to generate year data from 1900 to 2499
*/

create or replace function days_of_yr(yr int)
	returns int
	language plpgsql
  as
$$
declare
numdays int;
begin
  if is_leap(yr) = true then 
  	numdays := 366;
  else 
  	numdays := 365;
END if;
return numdays;
end;
$$;


create or replace function is_leap(yr int)
   returns bool
   language plpgsql
  as
$$
declare 
is_leap bool;
begin
  if (mod(yr, 400) = 0) and (mod(yr, 100) = 0) then
  is_leap := true;
elsif (mod(yr, 4) = 0) and (mod(yr, 100) != 0) then
  is_leap := true;
 else 
  is_leap := false;
END if;
return is_leap;
end;
$$;


create or replace procedure create_year_data(start_yr int, end_yr int)
language plpgsql
as $$
declare
-- variable declaration
begin
	for counter in start_yr..end_yr loop
	insert into years(ayear,
					is_leap,
					numdays)
	values (counter, 
		is_leap(counter),
		days_of_yr(counter));
   end loop;
-- stored procedure body
end; 
$$;

call create_year_data(1900, 2499);
