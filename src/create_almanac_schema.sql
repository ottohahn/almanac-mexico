/* PROGRAM create_almanac_schema.sql
   AUTHOR Otto Hahn Herrera
   DATE 2023-06-21
   PURPOSE To create a schema for developing an almanac
*/

CREATE SCHEMA dev AUTHORIZATION postgres;

CREATE TABLE dev.years (ayear integer not null primary key,
    is_leap bool,
    numdays integer);

CREATE TABLE dev.date_data(
    ayear integer,
    month integer, 
    mth_name varchar(12), 
    yyyymm integer,  
    yyyyww integer,
    mmww text,
    doy integer,
    dom integer,
    dow integer,
    julian bigint,
    day_name varchar(12),
    zodiac varchar(20), 
    full_date date primary key not null);


