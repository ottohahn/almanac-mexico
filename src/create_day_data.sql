/*
PROGRAM: create_day_data.sql
AUTHOR: Otto Hahn Herrera
DATE: 2023-06-29
PURPOSE: Create data for a given range of days
*/
-- for week of the month
-- https://wiki.postgresql.org/wiki/Date_WeekOfMonth

-- To generate yyyymm
create or replace function yearmonth(adate text)
returns integer
as $$
declare 
	yyyymm integer;
begin 
	yyyymm = concat(date_part('Year',adate::timestamp), 
	lpad(date_part('month',adate::timestamp)::text,2,'0'))::int;
	return yyyymm;
end;
$$ language plpgsql;

--create or replace function year_week
create or replace function yearweek(adate text)
returns integer
as $$
declare 
	yyyyww integer;
begin 
	-- note: this will give the incorrect answer for the first week of the year
	-- if it does not start on a Sunday, if month is January and week is 52 or 53,
	-- then use last year
	case 
		when (date_part('week',adate::timestamp) >= 51 
			and date_part('month',adate::timestamp) = 1) then
			yyyyww = concat(date_part('Year',adate::timestamp)-1, 
	lpad(date_part('week',adate::timestamp)::text,2,'0'))::int;
		else
			yyyyww = concat(date_part('Year',adate::timestamp), 
	lpad(date_part('week',adate::timestamp)::text,2,'0'))::int;
		end case;
	return yyyyww;
end;
$$ language plpgsql;

--create or replace function week_of_month
CREATE OR REPLACE FUNCTION weekmonth(adate text)
RETURNS text AS $$
DECLARE
    first_day date;
    first_day_weekday integer;
    day_of_month integer;
    diff integer;
    week_of_month integer;
BEGIN
    -- Step 1: Get the first day of the month
    first_day := DATE_TRUNC('month', adate::timestamp);

    -- Step 2: Get the day of the week for the first day of the month
    first_day_weekday := EXTRACT(DOW FROM first_day);

    -- Step 3: Get the day of the month for the given date
    day_of_month := EXTRACT(DAY FROM adate::timestamp);

    -- Step 4: Calculate the difference between the day of the month and the day of the week
    diff := day_of_month - first_day_weekday;

    -- Step 5: Divide the difference by 7 and round up
    week_of_month := CEIL(diff / 7.0);

    RETURN concat(lpad(date_part('Month',adate::timestamp)::text,2,'0'), 
	lpad(week_of_month::text,2,'0'))::text;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION get_zodiac_sign(date_str date)
RETURNS text AS $$
DECLARE
    given_date date;
    zodiac_sign text;
BEGIN
    -- Convert the date string to a date type
    given_date := date_str;

    -- Extract the month and day from the given date
    -- and determine the zodiac sign based on the month and day

    CASE
        -- January 20 - February 18
        WHEN EXTRACT(MONTH FROM given_date) = 1 AND EXTRACT(DAY FROM given_date) >= 20 OR
             EXTRACT(MONTH FROM given_date) = 2 AND EXTRACT(DAY FROM given_date) <= 18 THEN
            zodiac_sign := 'Aquarius';

        -- February 19 - March 20
        WHEN EXTRACT(MONTH FROM given_date) = 2 AND EXTRACT(DAY FROM given_date) >= 19 OR
             EXTRACT(MONTH FROM given_date) = 3 AND EXTRACT(DAY FROM given_date) <= 20 THEN
            zodiac_sign := 'Pisces';

        -- March 21 - April 19
        WHEN EXTRACT(MONTH FROM given_date) = 3 AND EXTRACT(DAY FROM given_date) >= 21 OR
             EXTRACT(MONTH FROM given_date) = 4 AND EXTRACT(DAY FROM given_date) <= 19 THEN
            zodiac_sign := 'Aries';

        -- April 20 - May 20
        WHEN EXTRACT(MONTH FROM given_date) = 4 AND EXTRACT(DAY FROM given_date) >= 20 OR
             EXTRACT(MONTH FROM given_date) = 5 AND EXTRACT(DAY FROM given_date) <= 20 THEN
            zodiac_sign := 'Taurus';

        -- May 21 - June 20
        WHEN EXTRACT(MONTH FROM given_date) = 5 AND EXTRACT(DAY FROM given_date) >= 21 OR
             EXTRACT(MONTH FROM given_date) = 6 AND EXTRACT(DAY FROM given_date) <= 20 THEN
            zodiac_sign := 'Gemini';

        -- June 21 - July 22
        WHEN EXTRACT(MONTH FROM given_date) = 6 AND EXTRACT(DAY FROM given_date) >= 21 OR
             EXTRACT(MONTH FROM given_date) = 7 AND EXTRACT(DAY FROM given_date) <= 22 THEN
            zodiac_sign := 'Cancer';

        -- July 23 - August 22
        WHEN EXTRACT(MONTH FROM given_date) = 7 AND EXTRACT(DAY FROM given_date) >= 23 OR
             EXTRACT(MONTH FROM given_date) = 8 AND EXTRACT(DAY FROM given_date) <= 22 THEN
            zodiac_sign := 'Leo';

        -- August 23 - September 22
        WHEN EXTRACT(MONTH FROM given_date) = 8 AND EXTRACT(DAY FROM given_date) >= 23 OR
             EXTRACT(MONTH FROM given_date) = 9 AND EXTRACT(DAY FROM given_date) <= 22 THEN
            zodiac_sign := 'Virgo';

        -- September 23 - October 22
        WHEN EXTRACT(MONTH FROM given_date) = 9 AND EXTRACT(DAY FROM given_date) >= 23 OR
             EXTRACT(MONTH FROM given_date) = 10 AND EXTRACT(DAY FROM given_date) <= 22 THEN
            zodiac_sign := 'Libra';

        -- October 23 - November 21
        WHEN EXTRACT(MONTH FROM given_date) = 10 AND EXTRACT(DAY FROM given_date) >= 23 OR
             EXTRACT(MONTH FROM given_date) = 11 AND EXTRACT(DAY FROM given_date) <= 21 THEN
            zodiac_sign := 'Scorpio';

        -- November 22 - December 21
        WHEN EXTRACT(MONTH FROM given_date) = 11 AND EXTRACT(DAY FROM given_date) >= 22 OR
             EXTRACT(MONTH FROM given_date) = 12 AND EXTRACT(DAY FROM given_date) <= 21 THEN
            zodiac_sign := 'Sagittarius';

        -- December 22 - January 19
        WHEN EXTRACT(MONTH FROM given_date) = 12 AND EXTRACT(DAY FROM given_date) >= 22 OR
             EXTRACT(MONTH FROM given_date) = 1 AND EXTRACT(DAY FROM given_date) <= 19 THEN
            zodiac_sign := 'Capricorn';

        -- Default case
        ELSE
            zodiac_sign := 'Unknown';
    END CASE;

    RETURN zodiac_sign;
END;
$$ LANGUAGE plpgsql;

-- To generate the dates
create or replace procedure fill_day_data(start_date text, end_date text)
language plpgsql    
as $$
begin
	with days as (
	SELECT t.day::date as full_date
	FROM generate_series(start_date::timestamp, end_date::timestamp, interval  '1 day')
                     AS t(day)
	)
	insert into dev.date_data (ayear,
	month,
	mth_name,
	yyyymm, 
	yyyyww,
	mmww,
	doy,
	dom,
	dow,
	julian,
	moon_phase,
	zodiac,
	day_name,
	full_date)
	(select 
		date_part('Year', days.full_date) as ayear, --year
		cast(date_part('Month', days.full_date) as int) as amonth, --month		
		to_char(days.full_date , 'Month') as mth_name, --month name
		yearmonth(days.full_date::text) as yyyymm, --yearmonth
		yearweek(days.full_date::text) as yyyyww, --yyyyww
		weekmonth(days.full_date::text) as mmww,-- mmww
		DATE_PART('doy', days.full_date) as doy, -- day of the year
		date_part('day', days.full_date) as dom, --day of the month
		date_part('isodow', days.full_date) as dow, -- day of the week
		cast(to_char(days.full_date, 'J') as bigint) as julian, --Julian day
		get_moon_phase(days.full_date) as moon_phase,-- Moon phase
		get_zodiac_sign(days.full_date) as zodiac,-- zodiac
		to_char(days.full_date , 'Day') as day_name, -- day name
		days.full_date -- full date
		from days);
end;
$$

call fill_day_data('2024-01-01', '2024-12-31');

