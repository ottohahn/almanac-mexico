# almanac-mexico

## Description

Database schema, functions and stored procedures to create an almanac, mostly date functions.

## Usage

1. run  the `create_almanac_schema.sql` file to create the database schema
2. edit the `crate_day_data.sql` in the line that calls the procedure
3. run the `create_day _data.sql` script
 
## Roadmap

- [X] Year
- [X] Month of the year
- [X] Month name
- [X] YYYYMM
- [X] YYYYWW
- [X] MMWW
- [X] Day of the week
- [X] Day of the month
- [X] Julian day
- [X] Day name
- [X] Full Date
- [X] Day of the year
- [X] Zodiac sign


## Authors and acknowledgment

Otto Hahn

